"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt

def brown1(Nt,M,dt=1):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    #Initialize variable
    X = np.zeros((M,Nt+1))

    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    for i in range(M):
        for j in range(Nt):
            X[i,j+1] = X[i,j] + np.sqrt(dt)*randn(1)

    Xm = np.mean(X,axis=0)
    Xv = np.var(X,axis=0)
    return X,Xm,Xv


def analyze(display):
    """Complete this function to analyze simulation error
    """
    Mvalues = np.linspace(0,100,101)
    Vvalues = np.zeros(101)
    Evalues = np.zeros(101)
    for M in range(0,101):
        X, Xm, Xv = brown1(100,M)
        Vvalues[M] = Xv[100]
        Evalues[M] = abs(100 - Xv[100])
        
    out = np.zeros((3,101))
    out[0,:]=Mvalues
    out[1,:]=Vvalues
    out[2,:]=Evalues
    
    if display == True:
        plt.loglog(out[0,:], out[1,:])
        plt.show()



X, Xm, Xv = brown1(100,200)
#plt.plot(X.T)
std = np.sqrt(Xv)
t = range(0,101)
#plt.plot(std)
